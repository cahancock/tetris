# Tetris Midterm
This is my midterm project for Vassar's CS203 Course.

## License
This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/3.0/](http://creativecommons.org/licenses/by-sa/3.0/).  
Works must be attributed to [Casey Hancock](http://github.com/caseyh).  