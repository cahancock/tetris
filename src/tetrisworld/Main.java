/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tetrisworld;

import tester.Tester;

/**
 *
 * @author Casey
 */
public class Main {
    public static void main (String[] argv) {

        // run the tests - showing only the failed test results
        TetrisExamples be = new TetrisExamples();
        Tester.runReport(be, false, false);

        // run the game
        TetrisWorld tw = new TetrisWorld();
        tw.bigBang(TetrisWorld.canvas_width,
                TetrisWorld.canvas_height,
                0.2);
    }
}
