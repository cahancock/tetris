/*
 * CMPU-203
 * Tetris game - Midterm.
 * Casey Hancock
 * This is a modified version of BlobWorldImp.
 */
package tetrisworld;

import javalib.colors.*;
import javalib.worldimages.*;

/**
 * An array of rows to store all of the blocks that are in rows.
 *
 * @author Casey Hancock
 */
public class Rows {

    /**
     * an array of rows
     */
    Row[] rows;

    /**
     * Constructor. Initializes an array of rows of blocks.
     *
     * @param num_columns Number of columns to store.
     * @param num_rows Number of rows to store.
     */
    public Rows(int num_columns, int num_rows) {
        rows = new Row[num_rows];
        for (int i = 0; i < num_rows; i++) {
            rows[i] = new Row(num_columns);
        }
    }

    /**
     * Checks whether there is a block at the location passed to it.
     *
     * @param column The column location to test.
     * @param row The row location to test.
     * @return True if there is a block, else false.
     */
    public boolean isFilled(int column, int row) {
        if (column < 0 || row < 0) {
            return false;
        }
        return rows[row].filled(column);
    }

    /**
     * Stores a block.
     *
     * @param block the block to store
     */
    public void land(Block block) {
        rows[block.row_ind].add(block);
    }

    /**
     * Checks if a block has landed in the topmost row.
     *
     * @param num_columns The number of columns in this world.
     * @return True if any column in the topmost row is filled, else false.
     */
    public boolean gameOverTop(int num_columns) {
        boolean ret = false;
        for (int i = 0; i < num_columns; i++) {
            if (isFilled(i, 0)) {
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Checks if a row is full and if so, clears it and shifts all rows above it
     * down both mathematically and within the array structure.
     *
     * @return True if a row was cleared, else false.
     */
    public boolean clearRows() {
        boolean ret = false;
        for (int i = 0; i < rows.length; i++) {
            if (rows[i].rowFull() && i > 0) {
                rows[i - 1].dropRow();
                rows[i].moveRowDown(rows[i - 1]);
                for (int j = i - 1; j > 0; j--) {
                    if (j > 0) {
                        rows[j - 1].dropRow();
                        rows[j].moveRowDown(rows[j - 1]);
                    }
                }
                ret = true;
            }

        }
        return ret;
    }

    /**
     * Returns the WorldImage that this class represents.
     *
     * @return The WorldImage that this class represents.
     */
    public WorldImage makeRows() {
        WorldImage image = new RectangleImage(new Posn(0, 0), TetrisWorld.canvas_width, TetrisWorld.canvas_height, new White());
        for (Row i : this.rows) {
            image = new OverlayImages(image, i.makeRow());
        }
        return image;
    }
}