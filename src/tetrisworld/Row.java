/*
 * CMPU-203
 * Tetris game - Midterm.
 * Casey Hancock
 * This is a modified version of BlobWorldImp.
 */
package tetrisworld;

import javalib.colors.*;
import javalib.worldimages.*;

/**
 * An array of blocks representing one row.
 *
 * @author Casey Hancock
 */
public class Row {

    /**
     * Field that holds this array of blocks.
     */
    public Block[] row;

    /**
     * Constructor. Creates a new empty array of blocks with a length of the
     * number of columns.
     *
     * @param columns The number of columns on the canvas.
     */
    public Row(int columns) {
        row = new Block[columns];
    }

    /**
     * Adds a block to this row.
     *
     * @param block A block not already in this array.
     */
    public void add(Block block) {
        row[block.column_ind] = block;
    }

    /**
     * Checks if the specified position/column in the current row is filled.
     *
     * @param column A column.
     * @return True if filled, else false.
     */
    public boolean filled(int column) {
        return !(row[column] == null);
    }

    /**
     * Checks if the current row is full.
     *
     * @return True if the row is full, else false.
     */
    public boolean rowFull() {
        for (int i = 0; i < this.row.length; i++) {
            if (!this.filled(i)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Fills each entry in this row with null.
     */
    public void emptyRow() {
        for (Block i : row) {
            i = null;
        }
    }

    /**
     * Mathematically drops each block in this row.
     */
    public void dropRow() {
        for (Block i : row) {
            if (!(i == null)) {
                i.forceBlockDrop();
            }
        }
    }

    /**
     * Populates this row with the blocks of the row passed to it.
     *
     * @param upper The row to be stored in this row.
     */
    public void moveRowDown(Row upper) {
        if (this.row.length == upper.row.length) {
            for (int i = 0; i < this.row.length; i++) {
                this.row[i] = upper.row[i];
            }
        }
    }

    /**
     * Returns this row's world image.
     *
     * @return A WorldImage of this row.
     */
    public WorldImage makeRow() {
        WorldImage image = new RectangleImage(new Posn(0, 0), TetrisWorld.canvas_width, TetrisWorld.block_width, new White());
        for (Block i : this.row) {
            if (!(i == null)) {
                image = new OverlayImages(image, i.blockImage());
            }
        }
        return image;
    }
}