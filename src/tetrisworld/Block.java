/*
 * CMPU-203
 * Tetris game - Midterm.
 * Casey Hancock
 * This is a modified version of BlobWorldImp.
 */
package tetrisworld;

import java.util.Random;
import javalib.colors.*;
import javalib.worldimages.*;

/**
 * Represents a block.
 *
 * @author Casey Hancock Original Copyright: Copyright 2012 Viera K. Proulx This
 * program is distributed under the terms of the GNU Lesser General Public
 * License (LGPL)
 */
public class Block {

    private Posn center;
    /**
     * This block's current row.
     */
    public int row;
    /**
     * This block's current column.
     */
    public int column;
    /**
     * This block's current row index (row - 1).
     */
    public int row_ind;
    /**
     * This block's current column index (column - 1).
     */
    public int column_ind;
    private static int width;
    private IColor color;

    /**
     * First constructor. Produces a block with characteristics passed to it.
     *
     * @param column This block's current column.
     * @param row This block's current row.
     * @param width This block's width.
     * @param color This block's color.
     */
    Block(int column, int row, int width, IColor color) {
        this.center = new Posn((column * width) - width / 2, (row * width) - width / 2);
        this.width = width;
        this.color = color;
        this.row = row;
        this.column = column;
        this.row_ind = row - 1;
        this.column_ind = column - 1;
    }

    /**
     * Second constructor. takes only basic information and randomly assigns the
     * rest.
     *
     * @param columns The number of columns that the block may fall within.
     * @param width This block's width.
     */
    Block(int columns, int width) {
        int randCol = randomColumn(columns);
        this.width = width;
        this.color = randomColor();
        this.row = 0;
        this.column = randCol;
        this.row_ind = this.row - 1;
        this.column_ind = this.column - 1;
        this.center = new Posn((this.column * width) - width / 2, (this.row * width) - width / 2);
    }

    /**
     * produces the image of this block at its current location and color.
     *
     * @return A rectangle image representing this block.
     */
    WorldImage blockImage() {
        return new OverlayImages(new RectangleImage(this.center, this.width, this.width, new Black()),
                new RectangleImage(this.center, this.width - 2, this.width - 2, this.color));
    }

    /**
     * Moves this block in some way: one step in the direction indicated, to the
     * bottom of legal positions, or change the block's color (easter egg - the
     * name is purposefully deceptive).
     *
     * @param ke A string representing a keystroke.
     */
    public void moveBlock(String ke) {
        // Move the block right, while not allowing collisions with other blocks
        // or allowing the block to exit the canvas.
        if (ke.equals("right") || ke.equals("a") || ke.equals("A")) {
            if (this.column < TetrisWorld.columns) {
                if (!(TetrisWorld.rows_array.isFilled(column_ind + 1, this.row_ind))) {
                    this.column++;
                    this.column_ind++;
                    this.center = new Posn((column * this.width) - this.width / 2, (row * this.width) - this.width / 2);
                }
            }
        } // Same as the above method, only left.
        else if (ke.equals("left") || ke.equals("d") || ke.equals("D")) {
            if (this.column > 1) {
                if (!(TetrisWorld.rows_array.isFilled(column_ind - 1, this.row_ind))) {
                    this.column--;
                    this.column_ind--;
                    this.center = new Posn((column * this.width) - this.width / 2, (row * this.width) - this.width / 2);
                }
            }
        } // Move the block down one space.
        else if (ke.equals("down") || ke.equals("s") || ke.equals("S")) {
            blockDrop();
        } // Move the block to the lowest legal position in the current column.
        else if (ke.equals("h") || ke.equals("H")) {
            hammer();
        } // change the color to Y, G, R, B, F (White), 0 (Black)
        // Yellow Y or y
        else if (ke.equals("Y") || ke.equals("y")) {
            this.color = new Yellow();
        } // Green G or g
        else if (ke.equals("G") || ke.equals("g")) {
            this.color = new Green();
        } // Red R or r
        else if (ke.equals("R") || ke.equals("r")) {
            this.color = new Red();
        } // Blue B or b
        else if (ke.equals("B") || ke.equals("b")) {
            this.color = new Blue();
        } // White F or f
        else if (ke.equals("F") || ke.equals("f")) {
            this.color = new White();
        } // Black 0
        else if (ke.equals("0")) {
            this.color = new Black();
        }
    }

    /**
     * Drops the block one row if it won't hit anything.
     */
    public void blockDrop() {
        if (this.row < TetrisWorld.rows && !(this.collision(TetrisWorld.rows_array, TetrisWorld.canvas_height))) {
            this.row++;
            this.row_ind++;
            this.center = new Posn((this.column * width) - width / 2, (this.row * width) - width / 2);
        }
    }

    /**
     * Drops the block one row, without checking for collisions.
     */
    public void forceBlockDrop() {
        this.row++;
        this.row_ind++;
        this.center = new Posn((this.column * width) - width / 2, (this.row * width) - width / 2);
    }

    /**
     * Drops the block to the lowest legal position in the current column.
     */
    private void hammer() {
        while (this.row < TetrisWorld.rows && !(this.collision(TetrisWorld.rows_array, TetrisWorld.canvas_height))) {
            this.forceBlockDrop();
        }
    }

    /**
     * Checks if the block hit another block or the end of the canvas.
     *
     * @param rows_array an array arrays that ultimately holds blocks.
     * @param canvas_height the height of the canvas
     * @return true if the block hit another block or the bottom of the canvas
     */
    public boolean collision(Rows rows_array, int canvas_height) {
        if (this.column_ind >= 0 && this.row_ind >= 0 && this.row_ind + 1 < rows_array.rows.length) {
            return rows_array.isFilled(this.column_ind, this.row_ind + 1);
        } else {
            return this.center.y + this.width / 2 >= canvas_height;
        }
    }

    /**
     * Helper method to generate a random number in the range 1 to # of columns.
     */
    private int randomColumn(int columns) {
        return new Random().nextInt(columns) + 1;
    }

    /**
     * Helper method to generate a random color: Blue, Green, Red, White, or
     * Yellow.
     */
    private IColor randomColor() {
        int num = new Random().nextInt(5);
        switch (num) {
            case 0:
                return new Blue();
            case 1:
                return new Green();
            case 2:
                return new Red();
            case 3:
                return new White();
            default:
                return new Yellow();
        }
    }
}
