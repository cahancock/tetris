/*
 * CMPU-203
 * Tetris game - Midterm.
 * Casey Hancock
 * This is a modified version of BlockWorldImp.
 */
package tetrisworld;

import java.awt.Color;
import javalib.colors.*;
import javalib.worldimages.*;
import tester.*;

/**
 * Examples and tests for imperative version of Block game. Original Copyright:
 * Copyright 2012 Viera K. Proulx This program is distributed under the terms of
 * the GNU Lesser General Public License (LGPL)
 */
class TetrisExamples {

    // examples of data for the Block class:
    TetrisWorld testWorld = new TetrisWorld();
    Block b1 = new Block(5, 5, 20, new White());
    Block b1left = new Block(4, 5, 20, new White());
    Block b1right = new Block(6, 5, 20, new White());
    Block b1down = new Block(5, 6, 20, new White());
    Block b1G = new Block(5, 5, 20, new Green());
    Block b1Y = new Block(5, 5, 20, new Yellow());
    Block b1B = new Block(5, 5, 20, new Blue());
    Block b1R = new Block(5, 5, 20, new Red());
    Block b10 = new Block(5, 5, 20, new Black());
    // examples of data for the TetrisWorld class:
    TetrisWorld b1w = new TetrisWorld(this.b1);
    TetrisWorld b1leftw = new TetrisWorld(this.b1left);
    TetrisWorld b1rightw = new TetrisWorld(this.b1right);
    TetrisWorld b1downw = new TetrisWorld(this.b1down);
    TetrisWorld b1Gw = new TetrisWorld(this.b1G);
    TetrisWorld b1Yw = new TetrisWorld(this.b1Y);
    TetrisWorld b1Bw = new TetrisWorld(this.b1B);
    TetrisWorld b1Rw = new TetrisWorld(this.b1R);
    TetrisWorld b10w = new TetrisWorld(this.b10);
    
    TetrisWorld testWorldend = new TetrisWorld(this.b1);
    
    Rows testRows = new Rows(20, 20);
    
    void reset() {

        // examples of data for the Block class:
        TetrisWorld testWorld = new TetrisWorld();
        this.b1 = new Block(5, 5, 20, new White());
        this.b1left = new Block(4, 5, 20, new White());
        this.b1right = new Block(6, 5, 20, new White());
        this.b1down = new Block(5, 6, 20, new White());
        this.b1G = new Block(5, 5, 20, new Green());
        this.b1Y = new Block(5, 5, 20, new Yellow());
        this.b1B = new Block(5, 5, 20, new Blue());
        this.b1R = new Block(5, 5, 20, new Red());
        this.b10 = new Block(5, 5, 20, new Black());


        // examples of data for the BlockWorld class:
        this.b1w = new TetrisWorld(this.b1);
        this.b1leftw = new TetrisWorld(this.b1left);
        this.b1rightw = new TetrisWorld(this.b1right);
        this.b1downw = new TetrisWorld(this.b1down);
        this.b1Gw = new TetrisWorld(this.b1G);
        this.b1Yw = new TetrisWorld(this.b1Y);
        this.b1Bw = new TetrisWorld(this.b1B);
        this.b1Rw = new TetrisWorld(this.b1R);
        this.b10w = new TetrisWorld(this.b10);
        
        this.testWorldend =
                new TetrisWorld(this.b1);
        
        testRows = new Rows(20, 20);
    }

    /**
     * test the method moveBlock in the Block class
     */
    void testMoveBlock(Tester t) {
        this.reset();
        this.b1.moveBlock("left");
        t.checkExpect(this.b1,
                this.b1left, "test moveBlock - left " + "\n");

        this.reset();
        this.b1.moveBlock("right");
        t.checkExpect(this.b1,
                this.b1right, "test moveBlock - right " + "\n");

        this.reset();
        this.b1.moveBlock("down");
        t.checkExpect(this.b1,
                this.b1down, "test moveBlock - down " + "\n");

        this.reset();
        this.b1.moveBlock("G");
        t.checkExpect(this.b1,
                this.b1G, "test moveBlock - G " + "\n");

        this.reset();
        this.b1.moveBlock("Y");
        t.checkExpect(this.b1,
                this.b1Y, "test moveBlock - Y " + "\n");

        this.reset();
        this.b1.moveBlock("R");
        t.checkExpect(this.b1,
                this.b1R, "test moveBlock - R " + "\n");
        
        this.reset();
        this.b1.moveBlock("B");
        t.checkExpect(this.b1,
                this.b1B, "test moveBlock - B " + "\n");
        
        this.reset();
        this.b1R.moveBlock("F");
        t.checkExpect(this.b1R,
                this.b1, "test moveBlock - F " + "\n");
        
        this.reset();
        this.b1.moveBlock("0");
        t.checkExpect(this.b1,
                this.b10, "test moveBlock - 0 " + "\n");
    }

    /**
     * test the method onKeyEvent in the BlockWorld class
     */
    void testOnKeyEvent(Tester t) {

        this.reset();
        this.b1w.onKeyEvent("left");
        t.checkExpect(this.b1w,
                this.b1leftw, "test moveBlock - left " + "\n");

        this.reset();
        this.b1w.onKeyEvent("right");
        t.checkExpect(this.b1w,
                this.b1rightw, "test moveBlock - right " + "\n");

        this.reset();
        this.b1w.onKeyEvent("down");
        t.checkExpect(this.b1w,
                this.b1downw, "test moveBlock - down " + "\n");

        this.reset();
        this.b1w.onKeyEvent("G");
        t.checkExpect(this.b1w,
                this.b1Gw, "test moveBlock - G " + "\n");

        this.reset();
        this.b1w.onKeyEvent("Y");
        t.checkExpect(this.b1w,
                this.b1Yw, "test moveBlock - Y " + "\n");

        this.reset();
        this.b1w.onKeyEvent("R");
        t.checkExpect(this.b1w,
                this.b1Rw, "test moveBlock - R " + "\n");
        
        this.reset();
        this.b1w.onKeyEvent("B");
        t.checkExpect(this.b1w,
                this.b1Bw, "test moveBlock - B " + "\n");
        
        this.reset();
        this.b1w.onKeyEvent("G");
        t.checkExpect(this.b1w,
                this.b1Gw, "test moveBlock - G " + "\n");
        
        this.reset();
        this.b1Gw.onKeyEvent("F");
        t.checkExpect(this.b1Gw,
                this.b1w, "test moveBlock - F " + "\n");
        
        this.reset();
        this.b1w.onKeyEvent("0");
        t.checkExpect(this.b1w,
                this.b10w, "test moveBlock - F " + "\n");
    }

    // test the method worldEnds for the class BlockWorld
    void testWorldEnds(Tester t) {

        this.reset();
        
//        t.checkExpect(this.testWorldend.worldEnds(),
//                new WorldEnd(true,
//                new OverlayImages(this.testWorldend.makeImage(),
//                new TextImage(new Posn(100, 40), "Block is outside the bounds",
//                Color.red))));

//        this.reset();
//        t.checkExpect(this.b1w.worldEnds(),
//                new WorldEnd(false, this.b1w.makeImage()));
    }
    
    void testRows(Tester t) {
        this.reset();
        t.checkExpect(this.testRows.rows.length == 20 &&
                this.testRows.rows[1].row.length == 20, "test Rows constructor");
    }
    
    void testRowsIsFilled(Tester t) {
        this.reset();
        this.testRows.rows[1].row[1] = this.b1;
        t.checkExpect(this.testRows.isFilled(1, 1), "test Rows isFilled");
    }
    
    void testRowsLand(Tester t) {
        this.reset();
        this.testRows.land(this.b1);
        t.checkExpect(this.testRows.rows[this.b1.row_ind].row[this.b1.row_ind], this.b1, "test Rows Land");
    }
    
    void testRowsOverTop(Tester t) {
        this.reset();
        Rows tempRow = new Rows(2, 2);
        Block tempBlockA = new Block(1, 2, 20, new White());
        tempRow.land(tempBlockA);
        Block tempBlockB = new Block(1, 1, 20, new White());
        tempRow.land(tempBlockB);
        t.checkExpect(tempRow.gameOverTop(2));
    }
    
    void testClearRows (Tester t) {
        this.reset();
        Rows tempRows = new Rows(2, 2);
        Block t1 = new Block(1, 2, 20, new White());
        tempRows.land(t1);
        Block t2 = new Block(2, 2, 20, new White());
        tempRows.land(t2);
        Block t3 = new Block(1, 1, 20, new White());
        tempRows.land(t3);
        tempRows.clearRows();
        Block t4 = new Block(1, 2, 20, new White());
        t.checkExpect(t3, t4, "test Rows clearRows");
    }
    
    void testAdd (Tester t) {
        this.reset();
        Row tempRow = new Row(2);
        Block t1 = new Block (1, 2, 20, new White());
        tempRow.add(t1);
        t.checkExpect(tempRow.row[0], t1, "test Row add");
    }
    
    void testRowFilled (Tester t) {
        this.reset();
        Row tempRow = new Row(2);
        Block t1 = new Block (1, 2, 20, new White());
        tempRow.add(t1);
        t.checkExpect(tempRow.filled(0) && !tempRow.filled(1), "test Row filled");
    }
    
    void testRowFull (Tester t) {
        this.reset();
        Rows tempRows = new Rows(2, 2);
        Block t1 = new Block(1, 2, 20, new White());
        tempRows.land(t1);
        Block t2 = new Block(2, 2, 20, new White());
        tempRows.land(t2);
        t.checkExpect(tempRows.rows[1].rowFull(),"test rowFull");
    }
    
    void testBlockDrop(Tester t) {
        this.reset();
        this.b1.blockDrop();
        t.checkExpect(this.b1, this.b1down, "test blockDrop");
    } 
    
    void testForceBlockDrop(Tester t) {
        this.reset();
        this.b1.blockDrop();
        t.checkExpect(this.b1, this.b1down, "test forceBlockDrop");
    } 
    
    void testCollision(Tester t) {
        this.reset();
        Rows tempRows = new Rows(2, 2);
        Block t1 = new Block(1, 2, 20, new White());
        tempRows.land(t1);
        Block t2 = new Block(1, 1, 20, new White());
        t.checkExpect(t2.collision(tempRows, 40) && t1.collision(tempRows, 40), "test Collision");
    } 
    
    
    /**
     * run the animation
     */
    TetrisWorld w1 =
            new TetrisWorld();
    TetrisWorld w2 =
            new TetrisWorld();
    TetrisWorld w3 =
            new TetrisWorld();

    // test that we can run three different animations concurrently
    // with the events directed to the correct version of the world
	/*
     boolean runAnimation = this.w1.bigBang(200, 300, 0.3); 
     boolean runAnimation2 = this.w2.bigBang(200, 300, 0.3); 
     boolean runAnimation3 = this.w3.bigBang(200, 300, 0.3); 
     (/
	

     /** main: an alternative way of starting the world and running the tests */
    
}