/*
 * CMPU-203
 * Tetris game - Midterm.
 * Casey Hancock
 * This is a modified version of BlobWorldImp.
 * 
 * Basic Tetris Game
 * Extra features: row clearing, "wasd" controls, hammer (quick) block drop with "h",
 *      random block drop column, block color changing ("Y", "R", "B", "G", "F", "0"),
 *      and pause "p".
 */
package tetrisworld;

import javalib.colors.*;
import javalib.impworld.*;
import javalib.worldimages.*;

/**
 * Represents the world of a simple Tetris Game. Original Copyright: Copyright
 * 2012 Viera K. Proulx This program is distributed under the terms of the GNU
 * Lesser General Public License (LGPL)
 */
public class TetrisWorld extends World {

    /**
     * The number of columns in this world.
     */
    public static int columns = 10;
    /**
     * The number of rows in this world.
     */
    public static int rows = 20;
    /**
     * The width of this block.
     */
    public static int block_width = 20;
    /**
     * The calculated width of this canvas based on columns, rows, and block
     * size.
     */
    public static int canvas_width = columns * block_width;
    /**
     * The calculated height of this canvas based on columns, rows, and block
     * size.
     */
    public static int canvas_height = rows * block_width;
    /**
     * Initializes an object that stores all blocks existing in this world.
     */
    public static Rows rows_array = new Rows(columns, rows);
    private static boolean pause = false;
    private int rows_cleared;
    private Block block;
    private WorldImage background = new RectangleImage(new Posn(0, 0), canvas_width, canvas_height, new White());

    /**
     * The constructor
     */
    public TetrisWorld() {
        super();
        this.block = new Block(columns, block_width);
    }
    /**
     * The constructor
     * @param block This block for this TetrisWorld.
     */
    public TetrisWorld(Block block) {
        super();
        this.block = block;
    }

    /**
     * Move the Block when the player presses a key
     *
     * @param ke A string representing a key event.
     */
    public void onKeyEvent(String ke) {
        if (ke.equals("x")) {
            this.endOfWorld("Goodbye");
        } else if (ke.equals("p") || ke.equals("P")) {
            this.pause = !this.pause;
        } else {
            if (!pause) {
                this.block.moveBlock(ke);
            }
        }
    }

    /**
     * On tick, drop the block if the game is not paused.
     */
    public void onTick() {
        if (pause) {
        } else {
            this.block.blockDrop();
        }

    }

    /**
     * Makes an image representing the current world - an image background and
     * the current block over it. Displays pause message if the game is paused.
     *
     * @return A WorldImage representing the current world.
     */
    public WorldImage makeImage() {
        if (pause) {
            return new OverlayImages(background,
                    new OverlayImages(this.block.blockImage(), new TextImage(new Posn(50, 20), "Paused.", new Red())));
        }
        if (!(this.block == null)) {
            return new OverlayImages(background, this.block.blockImage());
        } else {
            return background;
        }
    }

    /**
     * Process if the game has ended, a block has landed, or if the world should
     * keep ticking as is.
     *
     * @return WorldEnd object that determines how the world precedes, or
     * doesn't.
     */
    public WorldEnd worldEnds() {
        // if the block is outside the canvas, stop
        if (rows_array.gameOverTop(this.columns)) {
            this.background = new OverlayImages(background, this.block.blockImage());
            return new WorldEnd(true, new OverlayImages(this.background,
                    new OverlayImages(new RectangleImage(new Posn(50, 28), 100, 50, new Black()),
                    new OverlayImages(new TextImage(new Posn(50, 20), "GAME OVER", new Red()),
                    new TextImage(new Posn(50, 40), "SCORE: " + rows_cleared, new Red())))));
        } else if (this.block.collision(rows_array, canvas_height)) {
            rows_array.land(this.block);
            if (rows_array.clearRows()) {
                background = rows_array.makeRows();
                rows_cleared++;
            } else {
                background = new OverlayImages(background, this.block.blockImage());
            }
            this.block = new Block(this.columns, this.block_width);
            return new WorldEnd(false, this.background);
        } else {
            return new WorldEnd(false, this.makeImage());
        }
    }
    /**
     * Call to TetrisExamples to allow for testing.
     */
    public static TetrisExamples examplesInstance = new TetrisExamples();
}
